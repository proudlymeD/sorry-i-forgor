#!/bin/bash

#PBS -N process_VCF_file_step1
#PBS -o /home/zianl/1000_genomes_biallelic/
#PBS -l nodes=2:ppn=16
#PBS -l vmem=16gb
#PBS -m e
#PBS -M zian.liu@bcm.edu

cd ./1000_genomes_biallelic/

zgrep -v '##' ALL.chr5.shapeit2_integrated_v1a.GRCh38.20181129.phased.vcf.gz | cut -f 1-8 > chr5_info.txt
zgrep -v '##' ALL.chr5.shapeit2_integrated_v1a.GRCh38.20181129.phased.vcf.gz | cut -f 1-9 -n --complement | sed -e 's/0|0/0/g' -e 's/0|1/1/g' -e 's/1|0/1/g' -e 's/1|1/2/g' > chr5_freq.txt

# !Python3
# project_1000g_structure_value.py
# Zian Liu

"""
This is part two of the script, after the unix bedtools which is part of the pipeline.
This version of the script is specifically for the 1000 genomes project. Output is a single dataframe.
"""

import numpy as np
import pandas as pd
import re
from itertools import compress
from collections import Counter
import matplotlib.pyplot as plt

"""
The DNA structure references come from a R script I wrote using the *DNAshapeR* module. 
Note that all structures are included in the same dictionary 'Dict_full', cal them by referring to structure types.
Note that, the single output value summarizes the *mean* of values from all five locations that are affected by the SNV. 
Further analyses may include base-by-base. 
"""

# These are the references made by DNAshapeR, make each of them into a dictionary
Ref_dict = pd.ExcelFile('DNAshapeR_reference/ref_9mers_structure.xlsx')   # Read in as object
Dict_full = {}
Structures_all_list = ['HelT', 'Rise', 'Roll', 'Shift', 'Slide', 'Tilt', 'Buckle', 'Opening', 'ProT', 'Shear', 'Stagger', 'Stretch', 'MGW']
for structure_type in Structures_all_list:
    temp_dict = Ref_dict.parse(sheet_name = structure_type, index_col=0)
    Dict_full[structure_type] = dict(zip(list(temp_dict.index), temp_dict.values))

del Ref_dict

"""
Write a function about applying these libraries on a given sequence.
"""

def sequence_score(sequence, dictionary):
    """Given an input 9-mer (or any length) sequence and a 9-mer dictionary, calculate sequence score.
    Sequence should be of length 9 and should be comprised of uppercase A/C/T/G.
    Dictionary should ONLY be comprised of all combinations of 9-mer DNA sequences."""
    return list(dictionary[sequence])


"""
Next, define functions for calculating the structure scores, as well as making the final output. 
"""


def write_values_valonly(fasta_list, fasta_list_alt, dictionary, file_names_list):
    """This function is for batch processing the mean values.
    Fasta_list items are for before & after SNV.
    Dictionary is the list of dictionaries for translation.
    The output will be one Pandas dataframe ready for exporting."""
    if type(fasta_list) != list or type(fasta_list_alt) != list:
        raise ValueError('Input fasta lists are not in list formats!')
    for count_1 in range(len(fasta_list)):
        structure_output = pd.DataFrame(
            [sequence_score(fasta_list[count_1], dictionary) for count_1 in range(len(fasta_list))]
        )
        structure_output_alt = pd.DataFrame(
            [sequence_score(fasta_list_alt[count_1], dictionary) for count_1 in range(len(fasta_list_alt))]
        )
        output_dataframe = pd.concat([structure_output, structure_output_alt],
                                     axis=1, ignore_index=False)
        return output_dataframe


"""
The fasta sequences are generated from 'bedtools -getfasta' in unix. 
For each sequence, we will import it, check if there is anything wrong with the sequence, 
and then directly calculate the csv out of it. 
# Note that the "Chr19-coding" file has an error. 
"""

pop_select = 'AFR'
for chrom_no in [str(19)]:

    spam_file = 'biallelicSNV_chr' + str(chrom_no) + '_noncoding.bed.fa'
    filename = '_'.join(re.split("[_.]", spam_file)[1:3])

    # Import the names and fasta lists from the fasta file
    with open('outputs/fasta_files/' + spam_file, "r") as file_:
        Full_list = []
        for line_ in file_:
            Full_list.append(re.sub("\n", "", line_))
    Names_list = Full_list[0:len(Full_list):2]
    Fasta_list = Full_list[1:len(Full_list):2]
    Fasta_list = [foo.upper() for foo in Fasta_list]

    # Make the altered fasta file that changes the middle nucleotide as the SNV
    # Raise error if things go wrong
    Fasta_list_alt = []
    Substitution_class = []
    for i in range(len(Fasta_list)):
        mut = re.split('[>#]', Names_list[i])[1:3]
        if mut[0] != Fasta_list[i][4]:
            raise ValueError("9-mer sequence differs from eQTL reference! Gene_cell are " + filename)
        else:
            altered = list(Fasta_list[0])
            altered[4] = mut[1]
            Fasta_list_alt.append(''.join(altered))
            Substitution_class.append('>'.join(mut))

    # Import proportions info from info file
    DF_info = pd.read_csv('outputs/info_df/biallelicSNV_chr' + str(chrom_no) + '_noncoding_proportion.csv')

    # For proportions > 0.5, reverse major and minor alleles
    # Need to define which population are we reversing
    Sequence_to_reverse = list(DF_info[pop_select + '_full'].values > 0.5)
    Indices_to_reverse = [count for count, x in enumerate(Sequence_to_reverse) if x]
    for goo in Indices_to_reverse:
        Reverse_major = Fasta_list[goo]
        Reverse_minor = Fasta_list_alt[goo]
        Fasta_list[goo] = Reverse_minor
        Fasta_list_alt[goo] = Reverse_major
        DF_info.iloc[goo, 0:6] = 1 - DF_info.iloc[goo, 0:6].values
    if any(DF_info[pop_select + '_full'].values > 0.5):
        raise ValueError("Some minor alleles are still annotated as major alleles in the " + pop_select + " population!")

    DF_info["substitution"] = np.array(Substitution_class)

    # Calculate scores & record, attach to existing DF
    DF_final = DF_info
    for structure_type in Structures_all_list:
        temp_df = write_values_valonly(Fasta_list, Fasta_list_alt, Dict_full[structure_type], Names_list)
        # The following 4 lines of code re-name the column names to "Structure_n | Structure_r_n" formats
        colnames_repeat = int(len(temp_df.columns)/2)
        colnames_range = list(range(1, colnames_repeat+1)) * 2
        colnames_finn = [structure_type + '_'] * colnames_repeat + [structure_type + '_r_'] * colnames_repeat
        temp_df.columns = [(colnames_finn[k] + str(colnames_range[k])) for k in range(len(temp_df.columns))]
        # Add temp_df to DF_final
        DF_final = pd.concat([DF_final, temp_df], sort=False, axis=1)

    DF_final.to_csv("outputs/structures/biallelicSNV_chr" + str(chrom_no) + '_noncoding_structures.csv',
                    index=False)
    print('File ' + filename + 'processed. ')


"""
This will output multiple data frames in csv format about the before/after change for each cell type and gene. 
We can make further comparisons in the csv files. 
Before finishing, let's run a short analysis to see whether the same sequence + substitution sequence
    results in different MAFs. 
"""

Fasta_list_dup = [item for item, count in Counter(Fasta_list).items() if count > 1]
Fasta_list_alt_dup = [item for item, count in Counter(Fasta_list_alt).items() if count > 1]
len(Fasta_list_dup), len(Fasta_list_alt_dup)

foo = pd.DataFrame(zip(Fasta_list, Fasta_list_alt))
goo = np.array(DF_info[pop_select + '_full'])
list_dupseq_indices = list()
for i in range(len(Fasta_list_alt_dup)):
    temp_df = foo.loc[foo[1] == Fasta_list_alt_dup[i]]
    if len(set(temp_df[0])) == 1:
        list_dupseq_indices.append(list(temp_df.index))
len(list_dupseq_indices)

list_maf = np.array(DF_info[pop_select + '_full'])
plotting_x = list()
plotting_y = list()
for i in range(len(list_dupseq_indices)):
    plotting_y.extend(list_maf[list_dupseq_indices[i]])
    plotting_x.extend([i] * len(list_dupseq_indices[i]))

plt.figure()
#plt.hist(plotting_y, bins=21)
plt.scatter(x=plotting_x, y=plotting_y)
plt.title('MAF of identical sequences')
plt.xlabel('Identical sequences')
plt.ylabel('MAF')
plt.show()

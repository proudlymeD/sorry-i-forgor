#!/bin/bash

for filename in bed_files/*.bed;
	do filename_new=$filename.fa
	sudo bedtools getfasta -fi hg38_full/hg38.fa -bed $filename -name -fo $filename_new
	done

mv bed_files/*.fa fasta_files/

echo 'done'


# !Python3
# project_eQTL_1_getbed.py
# Zian Liu

"""
# eQTL DNA sequence project
In this project, we will evaluate how the different eQTLs for a single gene may contribute to gene expression
differently. We hypothesize this has something to do with the DNA structure, so we have generated the DNA structure
reference from DNAshapeR module in R and used it to analyze eQTLs in different genes.
First, import the libraries as the following:
"""

import pandas as pd
import re
from myvariant import MyVariantInfo
mv = MyVariantInfo()

"""
The eQTL files come from Fairfax et al. 2012, Nat Genetics; 44(5): 502–510. doi:10.1038/ng.2205.  
"""

# This is the cis eQTLs from the paper
eQTL_cis = pd.read_excel('Fairfax_2012_eQTL/41588_2012_BFng2205_MOESM2_ESM.xls', sheet_name=0)
# The following is the trans eQTLs, don't worry for now
#eQTL_trans = pd.read_excel('Fairfax_2012_eQTL/41588_2012_BFng2205_MOESM3_ESM.xls', sheet_name=0)

"""
Next, modify the eQTL dataframes so each one of them contains the useful data only
"""

eQTL_cis_trimmed = eQTL_cis[['SNP', 'CHR', 'BP', 'Cell', 'Gene', 'probe_start', 'probe_end', 'Strand']]
# What are the unique cell types & genes?
types_cell = list(set(eQTL_cis_trimmed['Cell']))
types_gene = list(set(eQTL_cis_trimmed['Gene']))   # Something went wrong for two genes; crop them out
types_gene = [x for x in types_gene if type(x) == str]
types_gene.sort()
print(types_cell, types_gene, sep='\n')

"""
Use "myvariant" to give coordinates of eQTLs. 
"""
# The following is a rescue script: if internet connection dropped, find out where to continue
#dropped = 'ZNF23'
#types_gene[dropped]

# Note: the following script may take a long time to run; use the range(3) trial first
for i in range(len(types_gene)):
#for i in range(3):
#for i in range(1298, len(types_gene)):
    gene = str(types_gene[i])
    for j in range(len(types_cell)):
        cell = types_cell[j]
        spam = eQTL_cis_trimmed.loc[(eQTL_cis_trimmed['Gene'] == gene) & (eQTL_cis_trimmed['Cell'] == cell)]
        ham_ = pd.Series.tolist(spam['SNP'])
        # Choose to use hg19 or hg38
        eggs = mv.querymany(ham_, scopes='dbsnp.rsid', fields='dbsnp', assembly='hg38')
        # Some queries may not come up; remove them in the following
        kill = []
        for k in range(len(eggs)):
            try:
                foo = eggs[k]['notfound']
                if foo:
                    kill.append(k)
            except:
                continue
        kill.sort(reverse=True)
        for k in kill:
            del eggs[k]
        # Summarize id, score, and strand information
        eggs_id = [eggs[foo]['_id'] for foo in range(len(eggs))]
        eggs_score = [eggs[foo]['_score'] for foo in range(len(eggs))]
        eggs_strand = [
            list(spam.loc[spam['SNP'] == eggs[foo]['query']]['Strand'])[0]
            for foo in range(len(eggs))]
        # With the sufficient information, generate BED files:
        # chromosome number  \t  start position  \t  end position  \t  name  \t  score  \t  strand
        eggs_sum = [
            re.split(':g.|[ACTG]>[ACTG]', eggs_id[k])[0] + '\t' +
            str(int(re.split(':g.|[ACTG]>[ACTG]', eggs_id[k])[1]) - 5) + '\t' +
            str(int(re.split(':g.|[ACTG]>[ACTG]', eggs_id[k])[1]) + 4) + '\t' +
            re.findall('[ACTG]>[ACTG]', eggs_id[k])[0] + "#" + cell + "#" + gene + "#" + eggs_id[k] + '\t' +
            str(eggs_score[k]) + '\t' +
            str(eggs_strand[k])
            for k in range(len(eggs_id))
        ]
        # Write a txt file, note the directory
        with open("outputs/bed_files/20200311_hg38/eQTL_"+gene+"_"+cell+".bed", "w") as file:
            for hoo in eggs_sum:
                file.write(f"{hoo}\n")
        file.close()

"""
Next, with the BED format files, find the FASTA sequences using linux bedtools.
This part of the pipeline is not in the script. Return later for more info. 
"""

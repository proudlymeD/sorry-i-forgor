#!/bin/bash

mkdir hg38_full
cd hg38_full

wget --timestamping 'ftp://hgdownload.cse.ucsc.edu/goldenPath/hg38/bigZips/latest/*'

echo 'download complete'

gunzip *.fa.gz
tar xvzf *.tar.gz

echo 'uncompression complete'

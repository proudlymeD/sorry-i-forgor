#!/bin/bash

wget --timestamping 'ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/data_collections/1000_genomes_project/release/20181203_biallelic_SNV/ALL.chr5.shapeit2_integrated_v1a.GRCh38.20181129.phased.vcf.gz'

echo 'download complete'
